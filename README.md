ics-ans-role-iocuser
=============================

Ansible role to install IOC users

Requirements
------------

- ansible >= 2.2
- molecule >= 1.24

Role Variables
--------------

None

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-iocuser
```

License
-------

BSD 2-clause
