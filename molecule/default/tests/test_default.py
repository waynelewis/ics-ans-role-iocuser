import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_iocusers(host):
    assert host.group("iocgroup").exists
    assert host.user("iocuser").exists
